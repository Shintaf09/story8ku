function setCookie(cname, cvalue, exdays = 365) {
	var d = new Date();
	d.setTime(d.getTime() + (exdays*24*60*60*1000));
	var expires = "expires="+ d.toUTCString();
	document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
}

function getCookie(cname) {
	var name = cname + "=";
	var decodedCookie = decodeURIComponent(document.cookie);
	var ca = decodedCookie.split(';');
	for(var i = 0; i <ca.length; i++) {
		var c = ca[i];
		while (c.charAt(0) == ' ') {
		  c = c.substring(1);
		}
		if (c.indexOf(name) == 0) {
			return c.substring(name.length, c.length);
		}
	}
	return "";
}

function darkMode() {
	$("body").css("background-color", "#25221E");
	$(".namaku").css ("color", "#DCDCDC");
	$(".text-center").css ("color", "#DCDCDC");
	$("#table2").css("background-color", "white");
	$("#table2").css("color", "black");
	$(".ui-accordion-header").css("background-color", "#F5F5F5");
	$(".ui-accordion-header").css("color", "black");
	$(".tabel").css("background-color", "white");
	$(".tabel").css("color", "black");
	$(".butt").css("background-color", "darkgrey");
	$(".input").css("background-color", "white");
	setCookie("colormode","dark");
}

function lightMode() {
	$("body").css("background-color","#FFFFF0");
	$(".namaku").css("color", "#808080");
	$(".text-center").css ("color", "#808080");
	$("#table2").css("background-color", "#25221E");
	$("#table2").css("color", "white");
	$(".tabel").css("background-color", "#25221E");
	$(".tabel").css("color", "white");
	$(".butt").css("background-color", "darkgrey");
	$(".input").css("background-color", "#25221E"); 
	setCookie("colormode","light");	
}

if (getCookie("colormode") === "light") {
	lightMode();
} else if (getCookie("colormode") === "dark") {
	darkMode();
}

$(document).ready(function() {
	$(".light").click(lightMode);
	$(".dark").click(darkMode);
	$(".accordion").accordion({
		collapsible: true
	});
});