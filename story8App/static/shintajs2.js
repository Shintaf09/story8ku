$(document).ready(function(){
    searchBook("Technology");
    $("#searchbook").change(function(){
        searchBook(document.getElementById("searchbook").value);
    });
});

function searchBook(toSearch) {
    $.getJSON(`https://www.googleapis.com/books/v1/volumes?q=${toSearch}`, function(result){
        let counter = 1;
        document.getElementsByTagName("table")[0].innerHTML = 
`
<thead>
<tr>
    <th scope="col">#</th>
    <th scope="col">Picture</th>
    <th scope="col">Title</th>
    <th scope="col">Authors</th>
</tr>
</thead>
<tbody>
</tbody>
`;
        result.items.forEach(value => {
            let row = document.createElement("tr");

            let data0 = document.createElement("td");
            data0.innerText = counter;
            row.appendChild(data0);

            let dataku = document.createElement("td");
            dataku.innerHTML = `<img src=${value.volumeInfo.imageLinks.thumbnail}>`;
            row.appendChild(dataku);

            let data1 = document.createElement("td");
            data1.innerText = value.volumeInfo.title;
            row.appendChild(data1);
            
            let data2 = document.createElement("td");
            data2.innerText = value.volumeInfo.authors.join(", ");
            row.appendChild(data2);

            console.log("Pencarian Berhasil");

            document.getElementsByTagName("tbody")[0].appendChild(row);
            ++counter;
        });
    });
}
